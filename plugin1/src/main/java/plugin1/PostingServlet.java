package plugin1;

import api.SampleService;

import com.atlassian.templaterenderer.TemplateRenderer;

import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


@Scanned
public class PostingServlet extends HttpServlet
{
    @ComponentImport
    private final TemplateRenderer templateRenderer;


    private final SampleService sampleService;


    @Inject
    public PostingServlet( final TemplateRenderer templateRenderer,
                           final SampleService sampleService)
    {
        this.templateRenderer = templateRenderer;
        this.sampleService = sampleService;
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        Map<String, Object> context = new HashMap<String, Object>();

        this.templateRenderer.render("/templates/submission.vm", context, response.getWriter());


    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        Map<String, Object> context = new HashMap<String, Object>();
        this.sampleService.setMessage(request.getParameter("data"));

        context.put("submitted", true);

        this.templateRenderer.render("/templates/submission.vm", context, response.getWriter());

    }
}
