package plugin1;

import api.SampleService;
import com.atlassian.plugin.spring.scanner.annotation.component.JiraComponent;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;

@ExportAsService
@JiraComponent
public class SampleServiceImpl implements SampleService
{
    String theMessage = null;

    public SampleServiceImpl()
    {
        this.theMessage = "";
    }

    public String fetchMessage()
    {
        return this.theMessage;
    }

    public void setMessage(String message)
    {
        this.theMessage = message;
    }
}
