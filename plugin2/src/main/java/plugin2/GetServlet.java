package plugin2;


import api.SampleService;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import javax.inject.Inject;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Scanned
public class GetServlet extends HttpServlet
{
    @ComponentImport
    private final TemplateRenderer templateRenderer;


    @ComponentImport
    private final SampleService sampleService;


    @Inject
    public GetServlet( final TemplateRenderer templateRenderer,
                           final SampleService sampleService)
    {
        this.templateRenderer = templateRenderer;
        this.sampleService = sampleService;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        Map<String, Object> context = new HashMap<String, Object>();

        context.put("message", this.sampleService.fetchMessage());
        this.templateRenderer.render("/templates/reader.vm", context, response.getWriter());


    }

}
